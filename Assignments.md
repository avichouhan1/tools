# Assignments for this course.

## Instructions

1. Each student is assigned to some TA. You need give read access to
   your TA and no one else so that he/she can clone your repo and
   evaluate it. You can find bitbucket usernames of all TAs in
   [List of TA][ta].

2. For each assignment, please make a set of small commits instead of
   one single commit. Each logical stuff you do can be in a separate
   commit.  The commit messages act as a documentation on history of
   the project and hence having good logical commit messages are
   important for the project.

3. If there is any clarifications/mistakes please use the issue tracker.

#### Repository Structure
```
<your repository>
    |
    |-- assignment1
    |       |-- <other files>
    |       |-- README.md
    |
    |--
    |
    |-- assignment5
    |       |-- <other files>
    |       |-- README.md
    |
    |--
```

## Assignment 1

**Deadline: 25th Jan 2015 (Sunday) midnight**


The goal of this assignment is to get you familiar with latex and some
of its features.

1. Create your resume.
    * Add basic information
    * Add a passport size photograph
    * Add your transcript in tabular format. Please follow the format
      that DOAA uses. The grades and subjects taken can be fictitious but
      the format should be that of DOAA.

2. Write an article on any *one* of the following
    * Define AP and GP and prove its summation formula.
    * Define the Harmonic number and Eulers integral representation of
    it.  <http://en.wikipedia.org/wiki/Harmonic_number>. Prove that
    the n-th Harmonic number H(n) is Order (log n). Since the function
    $f(x) = 1/x$ is a monotonically decreasing, the idea is to show
    that the integral from 0 to n of 1/x approximates the H(n) above
    and below. Compare the areas under and above the curve in the picture
	given in the wikipedia entry. Details are available
	at <https://math.dartmouth.edu/~m105f13/m105f13notes1.pdf> but
	you need to express it in your own words.



   In each case you need to give a reference to a book or url. This should
   use bibtex.


## Assignment 2

**Deadline: 1st Feb 2015 (Sunday) midnight**

Write a make file for building the latex documents that you did as part
of assignment 1. In particular, it should support the following targets.

1. `dvi` : This should build the dvi version of the document.
2. `pdf` : This should build the pdf version of the document.
3. `all` : Should build both the dvi and pdf version.
4. `clean` : should remove all intermediate files.

Make sure that your make file also takes care of running bibtex as
well.  to generate the references. The `clean` target should remove
all the intermediate files including the intermediate files produced
by the bibtex.

[ta]: https://bitbucket.org/ppk-teach/tools/wiki/List%20of%20TA
